<?php

/**
 * @file
 * Helper functions for the Important Information module.
 *
 * Use module_load_include('inc', 'important_information',
 * 'important_information')
 * to use these functions within another module.
 *
 * @ingroup important_information
 */

/**
 * Save important information settings as a theme.
 */
function _important_information_settings_theme($theme = "default", $settings = "") {
  // Unset Drupal fields.
  if (is_array($settings)) {
    variable_set('iis_body', $settings['body']['value']);
    variable_set('iis_format', $settings['body']['format']);
    unset($settings['visibility']);
    unset($settings['body']);
    unset($settings['pages']);
    unset($settings['roles']);
    unset($settings['regions']);
    unset($settings['theme']);
    unset($settings['new_theme']);
    unset($settings['delta']);
    unset($settings['module']);
    unset($settings['theme_name']);
    unset($settings['form_build_id']);
    unset($settings['form_token']);
    unset($settings['form_id']);
    unset($settings['op']);
  }
  $theme = str_replace(' ', '_', strtolower($theme));
  $iiThemeSettings = json_encode($settings);
  // Load all saved themes.
  $folderPath = DRUPAL_ROOT . "/" . drupal_get_path('module', 'important_information') . '/themes';
  $availableThemes = array();
  $defaultThemes = array('default', 'bottom', 'left', 'right', 'top');
  $isiContributor = user_access('important information contributor');
  if ($handle = opendir($folderPath)) {
    while (FALSE !== ($file = readdir($handle))) {
      if ('.' == $file || '..' == $file) {
        continue;
      }
      $savedFile = fopen($folderPath . '/' . $file, "r") or die("Unable to open file!");
      $savedSettings = fread($savedFile, filesize($folderPath . '/' . $file));
      fclose($savedFile);
      $basename = basename($file, '.json');
      $availableThemes[$basename] = $savedSettings;
    }
    closedir($handle);
  }
  if (!is_array($availableThemes) 
  || count($availableThemes) == 0 
  || !array_key_exists($theme, $availableThemes)
  || (array_key_exists($theme, $defaultThemes) && !$isiContributor)) {
    if (array_key_exists($theme, $defaultThemes)) {
      // Trying to override deafult theme.
      $theme = "$theme-custom";
    }
    // New theme.
    // Save the theme to a file.
    $filePath = DRUPAL_ROOT . "/" . drupal_get_path('module', 'important_information') . '/themes/' . $theme . '.json';
    $myfile = fopen($filePath, "w") or die("Unable to open file!");
    fwrite($myfile, $iiThemeSettings);
    fclose($myfile);
  }
  elseif ($settings != "") {
    if (array_key_exists($theme, $availableThemes)) {
      // Load files which are not updated.
      $prevValues = json_decode($availableThemes[$theme], TRUE);
      if ($settings['al_fs_icon'] == 0 && $prevValues['al_fs_icon'] != "") {
        $settings['al_fs_icon'] = $prevValues['al_fs_icon'];
      }
      if ($settings['al_fs_icon_back'] == 0 && $prevValues['al_fs_icon_back'] != "") {
        $settings['al_fs_icon_back'] = $prevValues['al_fs_icon_back'];
      }
      if ($settings['al_d_icon'] == 0 && $prevValues['al_d_icon'] != "") {
        $settings['al_d_icon'] = $prevValues['al_d_icon'];
      }
      if ($settings['al_d_icon_back'] == 0 && $prevValues['al_d_icon_back'] != "") {
        $settings['al_d_icon_back'] = $prevValues['al_d_icon_back'];
      }
      $iiThemeSettings = json_encode($settings);
    }
    // Save the theme to a file.
    $filePath = DRUPAL_ROOT . "/" . drupal_get_path('module', 'important_information') . '/themes/' . $theme . '.json';
    $myfile = fopen($filePath, "w") or die("Unable to open file!");
    fwrite($myfile, $iiThemeSettings);
    fclose($myfile);
    $availableThemes[$theme] = $iiThemeSettings;
  }
  variable_set('important_information__theme', $theme);
  return json_decode($availableThemes[$theme], TRUE);
}

/**
 * Generate custom CSS.
 */
function _important_information_css($settings) {
  // Variables.
  $section_class = '.block-important-information:not(.inline-element) .important-information';
  $body_class = "body.has-important-information.ii-{$settings['position']}";
  // Mixins.
  $border_style = _important_information_border_css($settings);
  $shadow_style = _important_information_shadow_css($settings);
  $headings_styles = _important_information_headings_css($settings, $section_class);
  $block_body_styles = _important_information_block_body_css($settings, $section_class);
  $color_styles = _important_information_color_css($settings, $section_class);
  // Positioning.
  $positioning_styles = _important_information_positioning_css($settings, $body_class, $section_class);
  $mobile_positioning_styles = _important_information_mobile_positioning_css($settings, $body_class, $section_class);
  // Spacing.
  $spacing_styles = _important_information_spacing_css($settings, $section_class);
  // Top bar.
  $topbar_styles = _important_information_block_header_topbar_css($settings, $section_class);
  // Sub bar.
  $subbar_styles = _important_information_block_header_subbar_css($settings, $section_class);
  // Action links.
  $action_links_styles = _important_information_action_links_css($settings);
  // Full screen window.
  $full_screen_styles = _important_information_full_screen_style($settings);
  // CSS.
  $css = <<<EOT
  @media screen {
  $headings_styles
  $topbar_styles
  $subbar_styles
  $section_class { $border_style }
  $section_class { $shadow_style }
  $spacing_styles
  $block_body_styles
  $color_styles
  $positioning_styles
  $action_links_styles
  $full_screen_styles
  $mobile_positioning_styles
  }
EOT;
  return $css;
}

/**
 * Generate custom top title CSS.
 */
function _important_information_block_header_topbar_css($settings, $section_class) {
  $output = "";
  $extra_output = "";
  $child_output = "";
  $element_class = "{$section_class} .information-top-bar";
  if ($settings['tb_title'] != "") {
    // Topbar css.
    $output = "{$element_class} { ";
    if ($settings['tb_margin_top'] != ""
    && $settings['tb_margin_right'] != ""
    && $settings['tb_margin_bottom'] != ""
    && $settings['tb_margin_left'] != "") {
      $output .= "margin: {$settings['tb_margin_top']} {$settings['tb_margin_right']} {$settings['tb_margin_bottom']} {$settings['tb_margin_left']};";
    }
    if ($settings['tb_padding_top'] != ""
    && $settings['tb_padding_right'] != ""
    && $settings['tb_padding_bottom'] != ""
    && $settings['tb_padding_left'] != "") {
      $child_output .= "{$element_class} > h4 { ";
      $child_output .= "padding: {$settings['tb_padding_top']} {$settings['tb_padding_right']} {$settings['tb_padding_bottom']} {$settings['tb_padding_left']};";
      $child_output .= "}";
    }
    if ($settings['tb_background_color'] != "") {
      $output .= "background-color: {$settings['tb_background_color']};";
    }
    if ($settings['tb_position'] != "") {
      $output .= "position: {$settings['tb_position']};";
    }
    if ($settings['tb_width'] != "") {
      $child_output .= "{$element_class} > h4 { ";
      $child_output .= "width: {$settings['tb_width']};";
      $child_output .= "margin: 0 auto;";
      $child_output .= "}";
    }
    if ($settings['tb_max_width'] != "") {
      $child_output .= "{$element_class} > h4 { ";
      $child_output .= "max-width: {$settings['tb_max_width']};";
      $child_output .= "}";
    }
    if ($settings['tb_height'] != "") {
      $output .= "height: {$settings['tb_height']};";
    }
    if ($settings['tb_text_indent'] != "") {
      $output .= "text-indent: {$settings['tb_text_indent']};";
    }
    if ($settings['tb_text_transform'] != "") {
      $output .= "text-transform: {$settings['tb_text_transform']};";
    }
    if ($settings['tb_text_align'] != "") {
      $output .= "text-align: {$settings['tb_text_align']};";
    }
    if ($settings['tb_font_color'] != "") {
      $output .= "color: {$settings['tb_font_color']};";
    }
    if ($settings['tb_font_weight'] != "") {
      $output .= "font-weight: {$settings['tb_font_weight']};";
    }
    if ($settings['tb_font_family'] != "") {
      $output .= "font-family: {$settings['tb_font_family']};";
    }
    if ($settings['tb_font_size'] != "") {
      $output .= "font-size: {$settings['tb_font_size']};";
    }
    $output .= "}";
    // Information body spacing.
    $body_element_class = "{$section_class} .information-body";
    if ($settings['tb_height'] != "" && $settings['body_height'] == "") {
      $spacing = $settings['tb_height'];
      if ($settings['ousp_top'] != "") {
        $spacing .= " - {$settings['ousp_top']}px";
      }
      $extra_output .= "{$body_element_class} { height: calc(100% - {$spacing}); }";
    }
    elseif ($settings['body_height'] != "") {
      $extra_output .= "{$body_element_class} { height: {$settings['body_height']}; }";
    }
    else {
      $extra_output .= "{$body_element_class} { height: 100%; }";
    }
  }
  return $output . $extra_output . $child_output;
}

/**
 * Generate custom sub-bar CSS.
 */
function _important_information_block_header_subbar_css($settings, $section_class) {
  $output = "";
  $child_output = "";
  $element_class = "{$section_class} .information-sub-bar";
  if ($settings['sb_title'] != "") {
    // Topbar css.
    $output = "{$element_class} { ";
    if ($settings['sb_margin_top'] != ""
    && $settings['sb_margin_right'] != ""
    && $settings['sb_margin_bottom'] != ""
    && $settings['sb_margin_left'] != "") {
      $output .= "margin: {$settings['sb_margin_top']} {$settings['sb_margin_right']} {$settings['sb_margin_bottom']} {$settings['sb_margin_left']};";
    }
    if ($settings['sb_padding_top'] != ""
    && $settings['sb_padding_right'] != ""
    && $settings['sb_padding_bottom'] != ""
    && $settings['sb_padding_left'] != "") {
      $child_output .= "{$element_class} > h4 { ";
      $child_output .= "padding: {$settings['sb_padding_top']} {$settings['sb_padding_right']} {$settings['sb_padding_bottom']} {$settings['sb_padding_left']};";
      $child_output .= "}";
    }
    if ($settings['sb_background_color'] != "") {
      $output .= "background-color: {$settings['sb_background_color']};";
    }
    if ($settings['sb_position'] != "") {
      $output .= "position: {$settings['sb_position']};";
    }
    if ($settings['sb_width'] != "") {
      $child_output .= "{$element_class} > h4 { ";
      $child_output .= "width: {$settings['sb_width']};";
      $child_output .= "margin: 0 auto;";
      $child_output .= "}";
    }
    if ($settings['sb_max_width'] != "") {
      $child_output .= "{$element_class} > h4 { ";
      $child_output .= "max-width: {$settings['sb_max_width']};";
      $child_output .= "}";
    }
    if ($settings['sb_height'] != "") {
      $output .= "height: {$settings['sb_height']};";
    }
    if ($settings['sb_text_indent'] != "") {
      $output .= "text-indent: {$settings['sb_text_indent']};";
    }
    if ($settings['sb_text_transform'] != "") {
      $output .= "text-transform: {$settings['sb_text_transform']};";
    }
    if ($settings['sb_text_align'] != "") {
      $output .= "text-align: {$settings['sb_text_align']};";
    }
    if ($settings['sb_font_color'] != "") {
      $output .= "color: {$settings['sb_font_color']};";
    }
    if ($settings['sb_font_weight'] != "") {
      $output .= "font-weight: {$settings['sb_font_weight']};";
    }
    if ($settings['sb_font_family'] != "") {
      $output .= "font-family: {$settings['sb_font_family']};";
    }
    if ($settings['sb_font_size'] != "") {
      $output .= "font-size: {$settings['sb_font_size']};";
    }
    $output .= "}";
  }
  return $output . $child_output;
}

/**
 * Generate custom borders CSS.
 */
function _important_information_border_css($settings) {
  $output = "";
  if ($settings['border_width'] != "" && $settings['border_color'] != "" && $settings['border_override'] == "") {
    $output = "border: {$settings['border_width']}px {$settings['border_style']} {$settings['border_color']};";
  }
  elseif ($settings['border_override'] != "") {
    $output = "{$settings['border_override']};";
  }
  return $output;
}

/**
 * Generate custom shadows CSS.
 */
function _important_information_shadow_css($settings) {
  $output = "";
  if ($settings['shadow_h_offset'] != "" && $settings['shadow_v_offset'] != "" && $settings['shadow_blur'] != ""
  && $settings['shadow_spread'] != "" && $settings['shadow_color'] != "") {
    $output = "box-shadow: {$settings['shadow_h_offset']} {$settings['shadow_v_offset']} {$settings['shadow_blur']} {$settings['shadow_spread']} {$settings['shadow_color']};";
  }
  return $output;
}

/**
 * Generate custom spacing CSS.
 */
function _important_information_spacing_css($settings, $section_class) {
  $output = "";
  if ($settings['insp_left'] > 0
      || $settings['insp_right'] > 0
      || $settings['insp_top'] > 0
      || $settings['insp_bottom'] > 0) {
    $output = "{$section_class} > h1 { padding: {$settings['insp_top']}px {$settings['insp_right']}px {$settings['insp_bottom']}px {$settings['insp_left']}px; }";
    $output .= "{$section_class} .information-body { padding: 0 {$settings['insp_right']}px {$settings['insp_bottom']}px; }";
  }
  return $output;
}

/**
 * Generate custom body CSS color.
 */
function _important_information_color_css($settings, $section_class) {
  $output = "";
  if ($settings['top_background_color'] != "") {
    $output = "$section_class .information-head { background-color: {$settings['top_background_color']}; }";
  }
  if ($settings['body_background_color'] != "") {
    $output .= "$section_class { background-color: {$settings['body_background_color']}; }";
  }
  return $output;
}

/**
 * Generate custom block body CSS.
 */
function _important_information_block_body_css($settings, $section_class) {
  $block_body_class = "$section_class .information-body";
  $block_body_styles = [];
  $output = "";
  if ($settings['body_font_color'] != "") {
    $block_body_styles['color'] = $settings['body_font_color'];
  }
  if ($settings['body_font_family'] != "") {
    $block_body_styles['font-family'] = $settings['body_font_family'];
  }
  if ($settings['body_font_size'] != "") {
    $block_body_styles['font-size'] = $settings['body_font_size'];
  }
  if ($settings['body_font_weight'] != "") {
    $block_body_styles['font-weight'] = $settings['body_font_weight'];
  }
  if ($settings['body_height'] != "") {
    $block_body_styles['height'] = $settings['body_height'];
  }
  if ($settings['body_width'] != "") {
    $block_body_styles['width'] = $settings['body_width'];
  }
  if ($settings['body_max_width'] != "") {
    $block_body_styles['max-width'] = $settings['body_max_width'];
  }
  if ($settings['body_margin'] != "") {
    $block_body_styles['margin'] = $settings['body_margin'];
  }
  if ($settings['body_padding'] != "") {
    $block_body_styles['padding'] = $settings['body_padding'];
  }
  if (count($block_body_styles) > 0) {
    $block_body_styles = _important_information_rules_to_string($block_body_styles);
    $output = "$block_body_class { $block_body_styles }";
  }
  return $output;
}

/**
 * Generate custom headings CSS.
 */
function _important_information_headings_css($settings, $section_class) {
  $title_styles = [];
  $output = "";
  if ($settings['headings_color'] != "") {
    $title_styles['color'] = $settings['headings_color'];
  }
  if ($settings['headings_font_family'] != "") {
    $title_styles['font-family'] = $settings['headings_font_family'];
  }
  for ($heading_count = 1; $heading_count <= 6; $heading_count++) {
    $heading_styles = array();
    $heading_important = ($settings["headings_h{$heading_count}_important"] == 1) ? "!important" : "";
    if ("" != $font_size = $settings["headings_h{$heading_count}_font_size"]) {
      $heading_styles['font-size'] = $font_size . $heading_important;
    }
    if ("" != $font_weight = $settings["headings_h{$heading_count}_font_weight"]) {
      $heading_styles['font-weight'] = $font_weight . $heading_important;
    }
    if ("" != $text_transform = $settings["headings_h{$heading_count}_text_transform"]) {
      $heading_styles['text-transform'] = $text_transform . $heading_important;
    }
    if ("" != $margin = $settings["headings_h{$heading_count}_margin"]) {
      $heading_styles['margin'] = $margin . $heading_important;
    }
    if ("" != $padding = $settings["headings_h{$heading_count}_padding"]) {
      $heading_styles['padding'] = $padding . $heading_important;
    }
    if ("" != $letter_spacing = $settings["headings_h{$heading_count}_letter_spacing"]) {
      $heading_styles['letter-spacing'] = $letter_spacing . $heading_important;
    }
    if ("" != $line_height = $settings["headings_h{$heading_count}_line_height"]) {
      $heading_styles['line-height'] = $line_height . $heading_important;
    }
    $heading_style = _important_information_rules_to_string($title_styles + $heading_styles);
    $output .= "{$section_class} .information-body h{$heading_count} { $heading_style }";
  }
  return $output;
}

/**
 * Generate positioning styling for the action links.
 */
function _important_information_action_links_css($settings) {
  $output = "";
  $element_class = "#block-important-information-important-information .action-links";
  // Main element positioning.
  if ($settings['al_position'] == "fixed" || $settings['al_position'] == "absolute") {
    $output .= "{$element_class} {";
    if ($settings['al_left'] != "") {
      $output .= "left: {$settings['al_left']};";
    }
    elseif ($settings['al_right'] != "") {
      $output .= "right: {$settings['al_right']};";
    }
    if ($settings['al_top'] != "") {
      $output .= "top: {$settings['al_top']};";
    }
    $output .= "position: {$settings['al_position']};";
    $output .= "}";
  }
  // Dismiss button styles.
  if ($settings['enable_dismiss'] == 1) {
    $output .= _important_information_action_links_button_style($settings, $element_class, "dismiss", "d");
  }
  // Full screen button styles.
  if ($settings['enable_full_screen'] == 1) {
    $output .= _important_information_action_links_button_style($settings, $element_class, "full-screen", "fs");
  }
  return $output;
}

/**
 * Get full screen window styles.
 */
function _important_information_full_screen_style($settings) {
  $fs_class = "body.important-information-full-screen #important-information-block";
  $output = "{$fs_class} {";
  if ($settings['al_fs_w_padding'] != "") {
    $output .= "padding: {$settings['al_fs_w_padding']};";
  }
  if ($settings['al_fs_w_width'] != "") {
    $output .= "width: {$settings['al_fs_w_width']};";
  }
  if ($settings['al_fs_w_height'] != "") {
    $output .= "height: {$settings['al_fs_w_height']};";
  }
  if ($settings['al_fs_w_top'] != "") {
    $output .= "top: {$settings['al_fs_w_top']};";
  }
  if ($settings['al_fs_w_left'] != "") {
    $output .= "left: {$settings['al_fs_w_left']};";
  }
  $output .= "}";
  return $output;
}

/**
 * Get action link button styles.
 */
function _important_information_action_links_button_style($settings, $element_class, $shortname, $abbreviation) {
  $output = "";
  $output_back = "";
  $dismiss_class = "{$element_class} .{$shortname}";
  $icon = file_load($settings["al_{$abbreviation}_icon"]);
  $icon_back = file_load($settings["al_{$abbreviation}_icon_back"]);
  $output .= "{$dismiss_class} {";
  $output_back .= "{$dismiss_class}.clicked {";
  if ("" != $width = $settings["al_{$abbreviation}_width"]) {
    $output .= "width: {$width};";
  }
  if ("" != $height = $settings["al_{$abbreviation}_height"]) {
    $output .= "height: {$height};";
  }
  if ("" != $background_color = $settings["al_{$abbreviation}_background_color"]) {
    $output .= "background-color: {$background_color};";
  }
  if ("" != $icon_color = $settings["al_{$abbreviation}_icon_color"]) {
    $output .= "color: {$icon_color};";
  }
  if ($icon && $settings["al_{$abbreviation}_use_icons"] == 1) {
    $icon_url = file_create_url($icon->uri);
    $output .= "background-image: url('{$icon_url}');";
    if ("" != $background_size = $settings["al_{$abbreviation}_icon_size"]) {
      $output .= "background-size: {$background_size};";
    }
  }
  if ($icon_back && $settings["al_{$abbreviation}_use_icons"] == 1) {
    $icon_url = file_create_url($icon_back->uri);
    $output_back .= "background-image: url('{$icon_url}');";
    if ("" != $background_size = $settings["al_{$abbreviation}_icon_size"]) {
      $output_back .= "background-size: {$background_size};";
    }
  }
  if ("" != $line_height = $settings["al_{$abbreviation}_line_height"]) {
    $output .= "line-height: {$line_height};";
  }
  if ("" != $text_align = $settings["al_{$abbreviation}_text_align"]) {
    $output .= "text-align: {$text_align};";
  }
  if ("" != $padding = $settings["al_{$abbreviation}_padding"]) {
    $output .= "padding: {$padding};";
  }
  if ("" != $font_weight = $settings["al_{$abbreviation}_font_weight"]) {
    $output .= "font-weight: {$font_weight};";
  }
  if ("" != $font_family = $settings["al_{$abbreviation}_font_family"]) {
    $output .= "font-family: {$font_family};";
  }
  if ("" != $float = $settings["al_{$abbreviation}_float"]) {
    $output .= "float: {$float};";
  }
  if ("" != $z_index = $settings["al_{$abbreviation}_z_index"]) {
    $output .= "z-index: {$z_index};";
  }
  $output .= "}";
  $output_back .= "}";
  return $output . $output_back;
}

/**
 * Generate custom positioning CSS.
 */
function _important_information_positioning_css($settings, $body_class, $section_class) {
  if ($settings['size'] == "") {
    return "";
  }
  // Positioning.
  $block_class = "#block-important-information-important-information";
  $content_class = "#important-information-block";
  $position_class = "$section_class.{$settings['position']}";
  $position_style = "";
  $body_margin_styles = "";
  $extra_styles = "";
  $button_position_styles = "";
  $side_spacing = intval($settings['ousp_left']) + intval($settings['ousp_right']);
  $height_spacing = intval($settings['ousp_top']) + intval($settings['ousp_bottom']);
  // Set the main positioning attributes.
  if ($settings['position_block_property'] != "unset" && $settings['position_block_property'] != "") {
    $extra_styles .= "$block_class { position: " . $settings['position_block_property'] . "; }";
    if ($settings['position_block_property'] == "fixed") {
      $extra_styles .= "$block_class { width: 100%; height: 100%; top: 0; left: 0; }";
    }
    if ($settings['position_block_property'] == "absolute" && $settings['position_content_property'] == "fixed") {
      // Place the fixed element to the left or the right depending on outer spacing settings.
      if ($settings['ousp_left'] != "") {
        $left_spacing = intval($settings['ousp_left']) . "px";
        $extra_styles .= "$block_class { top: 0; left: $left_spacing; }";
      } else if ($settings['ousp_right'] != "") {
        $right_spacing = intval($settings['ousp_right']) . "px";
        $extra_styles .= "$block_class { top: 0; right: $right_spacing; }";
      } else {
        $extra_styles .= "$block_class { top: 0; left: 0; }";
      }
    }
  }
  if ($settings['position_content_property'] != "unset" && $settings['position_content_property'] != "") {
    $extra_styles .= "$content_class { position: " . $settings['position_content_property'] . "; }";
    if ($settings['position_content_property'] == "fixed" && $settings['stb_enable'] == 1) {
      $extra_styles .= "$content_class { bottom: 0; }";
    }
  }
  // Add body spacing if valid.
  if ($settings['enable_extra_spacing'] == 1) {
    if ($settings['exsp_selector'] != "") {
      $extra_spacing_selector = $settings['exsp_selector'];
      $extra_spacing_type = $settings['exsp_spacing_type'];
      $extra_spacing_style = "$extra_spacing_selector { ";
      if ($settings['exsp_top'] != "") {
        $top_spacing = $settings['exsp_top'];
        $extra_spacing_style .= "$extra_spacing_type-top: $top_spacing;";
      }
      if ($settings['exsp_right'] != "") {
        $right_spacing = $settings['exsp_right'];
        $extra_spacing_style .= "$extra_spacing_type-right: $right_spacing;";
      }
      if ($settings['exsp_bottom'] != "") {
        $bottom_spacing = $settings['exsp_bottom'];
        $extra_spacing_style .= "$extra_spacing_type-bottom: $bottom_spacing;";
      }
      if ($settings['exsp_left'] != "") {
        $left_spacing = $settings['exsp_left'];
        $extra_spacing_style .= "$extra_spacing_type-left: $left_spacing;";
      }
      $extra_spacing_style .= "}";
      $extra_styles .= $extra_spacing_style;
    }
  }
  switch ($settings['position']) {
    case 'top':
      $position_style = "height: {$settings['size']};";
      if ($settings['ousp_left'] > 0
      || $settings['ousp_right'] > 0
      || $settings['ousp_top'] > 0
      || $settings['ousp_bottom'] > 0) {
        $settings['size'] = "calc({$settings['size']} + {$height_spacing}px)";
        $position_style .= "width: calc(100% - {$side_spacing}px); left: {$settings['ousp_left']}px; top: {$settings['ousp_top']}px;";
      }
      if ($settings['body_spacing_type'] != "hidden" && $settings['body_spacing_type'] != "none") {
        $body_margin_styles = "$body_class > #page-wrapper { {$settings['body_spacing_type']}-top: {$settings['size']}; }";
        $body_margin_styles .= "$body_class > #overlay-container { height: calc(100% - {$settings['size']});top: {$settings['size']}; }";
      }
      $button_position_styles = "$body_class #block-important-information-important-information .contextual-links-wrapper { top: calc({$settings['ousp_top']}px + 40px);right: {$settings['ousp_right']}px }";
      break;

    case 'bottom':
      $position_style = "height: {$settings['size']};";
      if ($settings['ousp_left'] > 0
      || $settings['ousp_right'] > 0
      || $settings['ousp_top'] > 0
      || $settings['ousp_bottom'] > 0) {
        $settings['size'] = "calc({$settings['size']} + {$height_spacing}px)";
        $position_style .= "width: calc(100% - {$side_spacing}px); left: {$settings['ousp_left']}px; bottom: {$settings['ousp_bottom']}px;";
      }
      if ($settings['body_spacing_type'] != "hidden" && $settings['body_spacing_type'] != "none") {
        $body_margin_styles = "$body_class > #page-wrapper { {$settings['body_spacing_type']}-bottom: {$settings['size']}; }";
        $body_margin_styles .= "$body_class > #overlay-container { height: calc(100% - {$settings['size']}); }";
      }
      $button_position_styles = "$body_class #block-important-information-important-information .contextual-links-wrapper { bottom: calc({$settings['size']} - 50px);top: auto;right: {$settings['ousp_right']}px }";
      break;

    case 'right':
      $position_style = "width: {$settings['size']};";
      if ($settings['ousp_left'] > 0
      || $settings['ousp_right'] > 0
      || $settings['ousp_top'] > 0
      || $settings['ousp_bottom'] > 0) {
        $side_spacing = intval($settings['ousp_left']) + intval($settings['ousp_right']);
        $settings['size'] = "calc({$settings['size']} + {$side_spacing}px)";
        $position_style .= "height: calc(100% - {$height_spacing}px); right: {$settings['ousp_right']}px; top: {$settings['ousp_top']}px;";
      }
      if ($settings['body_spacing_type'] != "hidden" && $settings['body_spacing_type'] != "none") {
        $body_margin_styles = "$body_class > #page-wrapper { {$settings['body_spacing_type']}-right: {$settings['size']}; }";
        $body_margin_styles .= "$body_class #toolbar { right: {$settings['size']}; margin: 0; padding: 0; }";
        $body_margin_styles .= "$body_class > #overlay-container { width: calc(100% - {$settings['size']}); }";
      }
      $button_position_styles .= "$body_class #block-important-information-important-information .contextual-links-wrapper { top: calc({$height_spacing}px + 50px);right: calc({$settings['ousp_right']}px + 60px); }";
      break;

    case 'left':
      $position_style = "width: {$settings['size']};";
      if ($settings['ousp_left'] > 0
      || $settings['ousp_right'] > 0
      || $settings['ousp_top'] > 0
      || $settings['ousp_bottom'] > 0) {
        $settings['size'] = "calc({$settings['size']} + {$side_spacing}px)";
        $position_style .= "height: calc(100% - {$height_spacing}px); left: {$settings['ousp_left']}px; top: {$settings['ousp_top']}px;";
      }
      if ($settings['body_spacing_type'] != "hidden" && $settings['body_spacing_type'] != "none") {
        $body_margin_styles = "$body_class > #page-wrapper { {$settings['body_spacing_type']}-left: {$settings['size']}; }";
        $body_margin_styles .= "$body_class #toolbar { left: {$settings['size']}; margin: 0; padding: 0; }";
        $body_margin_styles .= "$body_class > #overlay-container { width: calc(100% - {$settings['size']});left: {$settings['size']}; }";
      }
      $button_position_styles .= "$body_class #block-important-information-important-information .contextual-links-wrapper { left: calc({$settings['size']} - 75px);right: auto;top: {$height_spacing}px; }";
      break;

    case 'hidden':
      $body_margin_styles = "$body_class { margin: 0; }";
      $body_margin_styles .= "$body_class #page-wrapper { margin: 0; }";
      $position_style = "display: none;";
      break;

  }
  return <<<EOT
  $position_class { $position_style }
  $body_margin_styles
  $button_position_styles
  $extra_styles
EOT;
}

/**
 * Generate custom mobile positioning CSS.
 */
function _important_information_mobile_positioning_css($settings, $body_class, $section_class) {
  if ($settings['mobile_size'] == "") {
    return "";
  }
  // Positioning.
  $block_class = "#block-important-information-important-information";
  $content_class = "#important-information-block";
  $mobile_class = "@media (max-width: {$settings['mobile_breakpoint']}) { $body_class.mobile-ii-{$settings['mobile_position']} ";
  $position_class = "$mobile_class $section_class.{$settings['position']}";
  $position_style = "display: block;";
  $body_margin_styles = "";
  $button_position_styles = "";
  $mobile_styles = "";
  $extra_styles = "";
  $side_spacing = $settings['ousp_left_m'] + $settings['ousp_right_m'];
  $height_spacing = $settings['ousp_top_m'] + $settings['ousp_bottom_m'];
  // Set the main positioning attributes
  if ($settings['mobile_position_block_property'] != "unset" && $settings['mobile_position_block_property'] != "") {
    $extra_styles .= "$mobile_class $block_class { position: " . $settings['mobile_position_block_property'] . "; } }";
    if ($settings['mobile_position_block_property'] == "fixed") {
      $extra_styles .= "$mobile_class $block_class { width: 100%; height: 100%; top: 0; left: 0; } }";
    }
  }
  if ($settings['mobile_position_content_property'] != "unset" && $settings['mobile_position_content_property'] != "") {
    $extra_styles .= "$mobile_class $content_class { position: " . $settings['mobile_position_content_property'] . "; } }";
  }
  switch ($settings['mobile_position']) {
    case 'top':
      $settings['mobile_size'] = "calc({$settings['mobile_size']} + {$side_spacing}px)";
      if ($settings['ousp_left'] > 0
      || $settings['ousp_right'] > 0
      || $settings['ousp_top'] > 0
      || $settings['ousp_bottom'] > 0) {
        $position_style .= "width: calc(100% - {$side_spacing}px); height: calc({$settings['mobile_size']} - {$side_spacing}px); top: {$settings['ousp_top_m']}px; left: {$settings['ousp_left_m']}px;";
      }
      else {
        $position_style .= "height: {$settings['mobile_size']}; }";
        $mobile_styles = "$position_class { top: 0; left: 0; width: 100%; bottom: auto; } }";
      }
      if ($settings['body_spacing_type'] != "hidden") {
        $body_margin_styles = "$mobile_class > #page-wrapper { {$settings['body_spacing_type']}-top: {$settings['mobile_size']}; } }";
        $body_margin_styles .= "$mobile_class > #overlay-container { height: calc(100% - {$settings['mobile_size']});top: {$settings['mobile_size']}; width: 100%; } }";
        $body_margin_styles .= "$mobile_class #toolbar { right: 0; left: 0; top: {$settings['mobile_size']}; } }";
      }
      $button_position_styles .= "$mobile_class #block-important-information-important-information .contextual-links-wrapper { bottom: auto;top: auto; } }";
      break;

    case 'bottom':
      if ($settings['ousp_left'] > 0
      || $settings['ousp_right'] > 0
      || $settings['ousp_top'] > 0
      || $settings['ousp_bottom'] > 0) {
        $settings['mobile_size'] = "calc({$settings['mobile_size']} + {$side_spacing}px)";
        $position_style .= "width: calc(100% - {$side_spacing}px); height: calc({$settings['mobile_size']} - {$height_spacing}px); bottom: {$settings['ousp_bottom_m']}px; left: {$settings['ousp_left_m']}px;top: auto;";
      }
      else {
        $position_style .= "height: {$settings['mobile_size']};bottom: 0px;top: auto }";
        $mobile_styles = "$position_class { bottom: 0; left: 0; width: 100%; top: auto; } }";
      }
      if ($settings['body_spacing_type'] != "hidden") {
        $body_margin_styles = "$mobile_class > #page-wrapper { {$settings['body_spacing_type']}-bottom: {$settings['mobile_size']}; } }";
        $body_margin_styles .= "$mobile_class > #overlay-container { height: calc(100% - {$settings['mobile_size']}); width: 100%; top: {$settings['mobile_size']}; } }";
        $body_margin_styles .= "$mobile_class #toolbar { right: 0; left: 0; } }";
      }
      $button_position_styles .= "$mobile_class #block-important-information-important-information .contextual-links-wrapper { bottom: calc({$settings['mobile_size']} - 20px);top: auto;right: {$settings['ousp_right_m']}px; } }";
      break;

    case 'right':
      if ($settings['ousp_left'] > 0
      || $settings['ousp_right'] > 0
      || $settings['ousp_top'] > 0
      || $settings['ousp_bottom'] > 0) {
        $spacing_total = $settings['outer_spacing'] * 2;
        $settings['mobile_size'] = "calc({$settings['mobile_size']} + {$side_spacing}px)";
        $position_style .= "height: calc(100% - {$height_spacing}px); width: calc({$settings['mobile_size']} - {$side_spacing}px); top: {$settings['ousp_top_m']}px; right: {$settings['ousp_right_m']}px;";
      }
      else {
        $position_style .= "width: {$settings['mobile_size']}; }";
        $mobile_styles = "$position_class { top: 0; right: 0; height: 100%; } }";
      }
      if ($settings['body_spacing_type'] != "hidden") {
        $body_margin_styles = "$mobile_class { {$settings['body_spacing_type']}-right: {$settings['mobile_size']}; } }";
        $body_margin_styles .= "$mobile_class > #overlay-container { width: calc(100% - {$settings['mobile_size']}); height: 100%; right: {$settings['mobile_size']}; top: 0; } }";
        $body_margin_styles .= "$mobile_class #toolbar { right: {$settings['mobile_size']}; margin: 0; padding: 0; } }";
      }
      $button_position_styles .= "$mobile_class #block-important-information-important-information .contextual-links-wrapper { top: auto;right: {$settings['ousp_right_m']}px; } }";
      break;

    case 'left':
      if ($settings['ousp_left'] > 0
      || $settings['ousp_right'] > 0
      || $settings['ousp_top'] > 0
      || $settings['ousp_bottom'] > 0) {
        $settings['mobile_size'] = "calc({$settings['mobile_size']} + {$side_spacing}px)";
        $position_style .= "height: calc(100% - {$height_spacing}px); width: calc({$settings['mobile_size']} - {$side_spacing}px); top: {$settings['ousp_top_m']}px; left: {$settings['ousp_left_m']}px;";
      }
      else {
        $position_style .= "width: {$settings['mobile_size']}; }";
        $mobile_styles = "$position_class { top: 0; right: 0; height: 100%; } }";
      }
      if ($settings['body_spacing_type'] != "hidden") {
        $body_margin_styles = "$mobile_class { {$settings['body_spacing_type']}-left: {$settings['mobile_size']}; } }";
        $body_margin_styles .= "$mobile_class > #overlay-container { width: calc(100% - {$settings['mobile_size']}); height: 100%; left: {$settings['mobile_size']}; } }";
        $body_margin_styles .= "$mobile_class #toolbar { left: {$settings['mobile_size']}; right: auto; margin: 0; padding: 0; } }";
      }
      break;

    case 'hidden':
      $position_style = "display: none; }";
      $body_margin_styles = "$mobile_class { margin: 0; } }";
      $body_margin_styles .= "$mobile_class #page-wrapper { margin: 0; } }";
      break;

  }
  return <<<EOT
  $position_class { $position_style }
  $body_margin_styles
  $button_position_styles
  $mobile_styles
  $extra_styles
EOT;
}

/**
 * Turn an array into a stringified set of CSS properties.
 */
function _important_information_rules_to_string($rules) {
  $output = "";
  foreach ($rules as $key => $rule) {
    if ($rule != "") {
      $output .= "$key: $rule;";
    }
  }
  return $output;
}
