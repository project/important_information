## Introduction

The important information block is a module used to place an element
in your page for displaying information to the user.

This block is rendered in "fixed" position by default. This positioning
can be disabled via the Block Configurations page where you can access
all the settings for the block.

Currently, the section display properties can be adjusted from the block
configurations page. This reduces the amount of custom code needed to
create a section which looks good with your current theme.

The block comes with predefined themes. These themes are not meant to be
overwritten since these are a part of the module. You can create new
themes by changing the configurations of an existing theme and setting the
`new theme name` field to something else than blank. This will generate a
new .json file in your directory which has the set of saved configurations.


## Installation:

  * Installation is like all normal drupal modules:
   * Extract the 'important_information_block' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules).
   * Go to /admin/modules and enable the 'important_information_block' module.
   * Once enabled, configure the `Important Information` block to be in one of
    your theme regions in order to be visible for the user.

## Dependencies:

  * The Important Information is not dependent of other modules, but it is
    recommended to use it alongside a CKEditor module.

## Conflicts/known issues:

  * There are no current conflicts nor known issues with this module.
  
## Configuration

The following describes the configurations found at: 
  /admin/structure/block/manage/important_information/important_information/configure

### General

  * Theme.

    Lets you pick the currently active theme for the block. Any changes of configurations
    will override the selected theme if nothing is entered in the `New theme name` field.

  * Body.

    Enter the content which will appear within the block. Depending on your Wysiwig or
    CKEditor modules you will be able to enter any content.

  * Top bar.
  
    Enter the configurations relative to the bar which appears above the Important
    Information. You can configure the following settings for this group:

    * Margin : The outer spacing applied to the bar.
    * Padding : The inner spacing applied to the bar.
    * Title : The text appearing in the bar.
    * Height : The height applied to the bar as a style property.
    * Width : Set the appropriate width for the bar.
    * Max width: Contraint the width of your bar by a certain amount.
    * Text indent : The letter spacing applied to the header text.
    * Font color : The color used for the font on this section.
    * Font weight : The weight applied to the font on the title.
    * Font family : The font type being used for the title.
    * Font size : The size applied to the font on the header.
    * Background color : The color applied to the background of the bar.
    * Position : Set the positioning property for the bar.
    * Text transform : Set modifications to the header text such as italic.
    * Text align : Set the alignment of the bar's text to a specific direction.

  * Sub bar.

    Same configurations as the `Top bar`.

  * New theme name.

    Enter a name for the new theme configurations to be saved to. Leaving this
    blank results in overriding the currently selected theme.

### Action links.

  A group for the configurations on the links which appear on the top bar.
  The following configurations can be adjusted:

  * Position : The position for action links group in the top bar.
  * Top spacing : Spacing aplied to the top of the action links.
  * Left spacing : Spacing aplied to the left of the action links.
  * Right spacing : Spacing aplied to the right of the action links.
  
  * Dismiss button.

    A group of configurations for the block dismiss button activator.

    * Enable button : Enable/disable the button itself with a checkbox.
    * Text : Text applied to the button itself.
    * Width : The width styling which should be applied to the button.
    * Height : The height styling which should be applied to the button.
    * Font-awesome class : Use to implement an icon to your button. Ie: fa-plus
    * Font-awesome class (back) : Use to implement an icon which appears after
      you clicked the main button.
    * Use icons : A checkbock which lets you enable/disable the use of image assets.
    * Icon : A field which lets you submit an image for the icon which is used.
    * Icon (back) : The icon which should be used when the button has been already clicked.
    * Icon size : The size property value applied to the section. Ie: cover.
    * Background color : The color of the background applied to the button.
    * Icon color : The color applied to the icon if it's a FontAwesome icon.
    * Line height : Adjust how separate should one line be from another.
    * Alignment : The alighment for the icon within the button.
    * Padding : Lets you set the inner spacing for the button.
    * Font weight : Enter the weight property applied to the button.
    * Font family : The font type being used for the title.
    * Alignment : Set the float property of the button. Ie: float-right.
    * Z-Index : The hierarchical precedence of this element over the others.

  * Full screen button.

    This button is used to expand the block to cover a specific part of the screen.
    Same settings as the previous one, but with extra:

    * Padding : The padding applied to the full screen window which opens when the full screen is activated.
    * Width : The width applied to the full screen window which opens when the full screen is activated.
    * Height : The height applied to the full screen window which opens when the full screen is activated.
    * Top : The top spacing applied to the full screen window which opens when the full screen is activated.
    * Left : The left spacing applied to the full screen window which opens when the full screen is activated.

### Style.

  The style properties give the user absolute control over how their Important Information looks like in the page.
  The following fields can be adjusted under this group:

  * Display.

    Configure the positioning of the block in the page in mobile and desktop devices.

    * Desktop position : Set the default position of the block for the user in the page.
    * Desktop section size : Set the size of the element within the page while on desktop devices.
    * Mobile position : Set the position where the block will appear on the page for mobile devices.
    * Mobile section size : Set the size of the block when on mobile devices.
    * Mobile breakpoint : The width of the browser which toggles the mobile properties.
    * Inner spacing: desktop/mobile : The spacing applied to the block from its edge.
    * Outer spacing: desktop/mobile : The spacing applied to the block from the content.
    * Body spacing type : The CSS property used to add spacing (margin, padding).
    * Enable inline positioning : Make the block go from fixed position to relative, as a part of the content. 
      The triggering element is defined below.
    * Inline position selector : Select the element which when visible will make the block behave as a relative
      positioned element.

  * Color.

    Define the color used for the sections of the block.

    * Top: background color : The background color applied to the topbar of the block.
    * Body: background color : The background color of the body itself for the block.

  * Border.

    The properties which define the border for the block.

    * Color : The border color.
    * Style : The border style (all valid CSS properties listed).
    * Width : The width of the border line.
    * Border override : Enter the literal border style if you need to use advanced properties.
  
  * Shadows.

    The properties applied to the shadow style for the block.

    * Horizontal offset : The shadow horizontal offset which displaces the shadow right/left.
    * Vertical offset : The shadow vertical offset which displaces the shadow up/down.
    * Blur : The blur applied to the shadow which makes it more/less transparent.
    * Spread : The spread of shadow applied to scatter the effect.
    * Color : The color of the shadow.
  
  * Headings.

    The font properties for any HTML h1, h2, h3, h4, h5 and h6 elements.

    * Color : The color applied to the heading elements.
    * Font family : The font family applied to the headings.
    
    * h1/2/3/4/5/6

      The set of properties which can be defined individually per heading type.

      * Font size : The size of the font applied to the heading.
      * Font weight : The boldness applied to the font.
      * Text transform : Make the text be lowercase, uppercase, etc.
      * Margin : The margin applied to the heading element.
      * Padding : The padding applied to the heading element.
      * Letter spacing : The letter spacing applied to each letter of the heading.
      * Line height : The space left between line and line.
      * Enforce styles : Apply !important to the configurations set above.
  
  * Body.

    Properties applied to the body of the block. These settings are not enforced with !important.

    * Font color : The color of the font for text within the body.
    * Font family : The family of font used for text within the body.
    * Font size : The size of font used within the body.
    * Font weight : How bold the font appears within the block.
    * Height : The height applied to the body. This can be useful for when there are positioning issues.
    * Width : The width of the body within the block.
    * Max width : Constrain the width of the block by a specific value.
    * Margin : The margin applied to the body (center it with `0 auto`);
    * Padding : Add some space between the text and the border of the block.

## Maintainers

Esteban Arias (esteban.arias) - https://www.drupal.org/u/estebanarias

## Support

Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/important_information

