<?php

/**
 * @file
 * Helper functions for the Important Information module.
 *
 * Use module_load_include('inc', 'important_information',
 * 'important_information.import')
 * to use these functions within another module.
 *
 * These are used for importing the settings as a JSON
 * string from the CMS.
 *
 * @ingroup important_information
 */

/**
 * Settings import form fields.
 */
function important_informaton_import_settings_form($form, $form_state) {
  $form['json_input'] = array(
    '#type' => 'textarea',
    '#title' => t('Themes JSON'),
    '#description' => t('Paste the output found on the export page for the Important Information block.<br>Please remember that the current settings will be overwritten, and this action cannot be reverted.<br>If you want to keep your old settings, export them and save them locally before importing new ones.'),
    '#default_value' => "",
    '#rows' => 30,
  );
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Import settings'),
  );
  $form['#validate'][] = 'important_information_import_settings_form_validate';
  $form['#submit'][] = "important_information_import_settings_form_submit";
  return $form;
}

/**
 * Settings import form validation.
 */
function important_information_import_settings_form_validate($form, &$form_state) {
  $json_input = json_decode($form_state['values']['json_input']);
  if (!is_string($form_state['values']['json_input']) ||
  !is_object($json_input)) {
    form_set_error('json_input', t('Please make sure to use a valid JSON string taken from the export page.'));
  }
}

/**
 * Settings import form submission.
 */
function important_information_import_settings_form_submit($form, &$form_state) {
  variable_set('important_information__themes', $form_state['values']['json_input']);
  drupal_set_message(t('The settings have been updated.'));
}
