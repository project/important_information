<?php

/**
 * @file
 * Important Information block template.
 *
 * The file renders the important information and the actions enabled in the
 * block configuration page.
 *
 * Available variables:
 *
 * - $title: The title of the section available for the user.
 * - $copy: The copy which appears within the important information
 * section.
 * - $position: The position CSS class of the block in the page.
 * - $mobile_position: The mobile position CSS class of the block in the page.
 */
$action_links_html = "";
if ($enable_dismiss == 1 || $enable_full_screen == 1) {
  $action_links_html .= "<div class='action-links'>";
  if ($enable_dismiss == 1) {
    $dismissContent = "";
    if ($al_d_class != "") {
      $al_d_class .= " fas fa";
      $dismissContent = "<i class='{$al_d_class}'></i>";
    } else {
      $dismissContent = $al_d_text;
    }
    $action_links_html .= "<span class='dismiss' ori-fa-class='{$al_d_class}' back-fa-class='{$al_d_class_back}' style='display:none;'>{$dismissContent}</span>";
  }
  if ($enable_full_screen == 1) {
    $fsContent = "";
    if ($al_fs_class != "") {
      $al_fs_class .= " fas fa";
      $fsContent = "<i class='{$al_fs_class}'></i>";
    } else {
      $fsContent = $al_fs_text;
    }
    $action_links_html .= "<span class='full-screen' ori-fa-class='{$al_fs_class}' back-fa-class='{$al_fs_class_back}' style='display:none;' full-screen-open-text='{$al_fs_text_open}'>{$fsContent}</span>";
  }
  $action_links_html .= "</div>";
}
?>
<div class="important-information <?php print $position; ?> mobile-<?php print $mobile_position; ?> display-<?php print $position_content_property; ?>" id="important-information-block">
  <?php if ($tb_title != "") { ?>
    <div class="information-top-bar">
      <h4><span><?php print $tb_title; ?></span><?php print $action_links_html; ?></h4>
    </div>
    <?php if ($sb_title != "") { ?>
      <div class="information-sub-bar">
        <h4><span><?php print $sb_title; ?></span></h4>
    </div>
    <?php } ?>
  <?php } else { ?>
    <?php print $action_links_html; ?>
  <?php } ?>
  <div class="information-body"><?php print variable_get('iis_body'); ?></div>
</div>
