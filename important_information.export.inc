<?php

/**
 * @file
 * Helper functions for the Important Information module.
 *
 * Use module_load_include('inc', 'important_information',
 * 'important_information.import')
 * to use these functions within another module.
 *
 * These are used for importing the settings as a JSON
 * string from the CMS.
 *
 * @ingroup important_information
 */

/**
 * Generate the fields for the export form.
 */
function important_informaton_export_settings_form($form, $form_state) {
  $form['json_output'] = array(
    '#type' => 'textarea',
    '#title' => t('Themes JSON'),
    '#description' => t('The currently saved settings for Important Information themes.<br>You can make modifications from here if you want to rename a theme, or override the fields if necessary.<br>Make a backup before changing the values here!'),
    '#default_value' => variable_get('important_information__themes'),
    '#rows' => 30,
  );
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Update settings'),
  );
  $form['#validate'][] = 'important_information_export_settings_form_validate';
  $form['#submit'][] = "important_information_export_settings_form_submit";
  return $form;
}

/**
 * Validate the json used at the import functionality.
 */
function important_information_export_settings_form_validate($form, &$form_state) {
  $json_input = json_decode($form_state['values']['json_output']);
  if (!is_string($form_state['values']['json_output']) ||
  !is_object($json_input)) {
    form_set_error('json_output', t('Please make sure to use a valid JSON string.'));
  }
}

/**
 * Save the new theme settings.
 */
function important_information_export_settings_form_submit($form, &$form_state) {
  variable_set('important_information__themes', $form_state['values']['json_output']);
  drupal_set_message(t('The settings have been updated.'));
}
