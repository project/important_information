(function($) {
  
  Drupal.ImportantInformationAdminForm = {};

  Drupal.ImportantInformationAdminForm.set_settings = function(settings){
    settings = settings;
    var settingsArray = Object.entries(settings).map(([key, value]) => ({key,value}));
    settingsArray.forEach(element => {
      if (element.key == "body") {
        jQuery('#edit-body-value').html(element.value.value);
      }
      element.key = element.key.replace(/_/g, '-');
      var formElement = jQuery('#edit-'+element.key);
      if (formElement.length != 0) {
        var formElementType = formElement.attr("tagName").toLowerCase();
        switch (formElementType) {
          case 'input':
          case 'select':
            if (formElement.attr("type") == "checkbox") {
              if ((formElement.is(":checked") && element.value == 0)
              || (!formElement.is(":checked") && element.value == 1)) {
                formElement.click();
              }
            } else {
              formElement.val(element.value);
            }
            break;
          default:
            break;
        }
      }
    });
  }

  // Toggle the settings from a select.
  Drupal.ImportantInformationAdminForm.switch_settings = function(){
    $('select#edit-theme').change(function(){
      $("select#edit-theme option:selected").each(function() {
        var selectedTheme = $(this).text();
        var themes = Drupal.settings.important_information.themes;
        if (themes[selectedTheme]) {
          Drupal.ImportantInformationAdminForm.set_settings(themes[selectedTheme]);
        }
      });
    });
  }

  Drupal.behaviors.importantInformationAdminForm = {
    attach: function(context, settings) {
      $('select#edit-theme', context).once('switch-important-information-values', function() {
        Drupal.ImportantInformationAdminForm.switch_settings();
      });
    }
  };
  
  })(jQuery);
  