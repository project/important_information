(function($) {
  
  Drupal.ImportantInformation = {};

  // Toggle the block display.
  Drupal.ImportantInformation.toggle_display = function(){
    $('#block-important-information-important-information .action-links .dismiss').show().click(function(){
      $('body').removeClass('has-important-information');
      $('#block-important-information-important-information').hide();
      $(window).resize();
    });
  }

  // Toggle sticky position.
  Drupal.ImportantInformation.stick_it = function() {

    if ($("body").hasClass("bw-desktop") || $("body").hasClass("bw-tablet")) {

      var $stickElement = $('div#block-important-information-important-information');
      if ($stickElement.length && Drupal.settings.important_information.enable_sticky) {
        var $window = jQuery(window);
        var isiContBlock = $('div#important-information-block');
  
        var headerBlock = Drupal.settings.important_information.sticky_header_selector;
        var footerWrp = Drupal.settings.important_information.sticky_footer_selector;
        var header = $(headerBlock);
        //variables required for calculating height sidebar and isi content
        var windowHeight = $window.height();
        var headerHeight = header.innerHeight();
        if (Drupal.settings.important_information.sticky_header_height != "") {
          headerHeight = Drupal.settings.important_information.sticky_header_height;
        }
  
        var onLoadIiHeight = windowHeight - headerHeight;
        isiContBlock.css({height: onLoadIiHeight});

        $(window).scroll(Drupal.ImportantInformation.throttle(function (e) {
          var header = $(headerBlock);
          var $window = jQuery(window);
          var windowHeight = $window.height();
          var headerHeight = header.innerHeight();
          var windowTopPosition = $window.scrollTop();
          var topSpacing = Drupal.settings.important_information.sticky_top_spacing;

          if (!Drupal.ImportantInformation.is_scrolled_into_view(headerBlock) && !Drupal.ImportantInformation.is_scrolled_into_view(footerWrp)) {
            // No header and no footer.
            $stickElement.addClass('sticky');
            isiContBlock.css({height: windowHeight});
            if (Drupal.settings.important_information.sticky_footer_scroll == 1) {
              isiContBlock.css({bottom: ''});
            }
          }
          else if (Drupal.ImportantInformation.is_scrolled_into_view(headerBlock) && !Drupal.ImportantInformation.is_scrolled_into_view(footerWrp)) {
            // Header visible but no footer visible.
            $stickElement.removeClass('sticky');
            isiContBlock.css({height: windowHeight - headerHeight + windowTopPosition});
          }
          else if (!Drupal.ImportantInformation.is_scrolled_into_view(headerBlock) && Drupal.ImportantInformation.is_scrolled_into_view(footerWrp)) {
            // Header not visible but footer visible.
            $stickElement.removeClass('sticky');
            var footerOffset = Math.abs($(footerWrp).offset().top - windowTopPosition - windowHeight);
            isiContBlock.css({height: windowHeight - topSpacing});
            if (Drupal.settings.important_information.sticky_footer_scroll == 1) {
              isiContBlock.css({bottom: footerOffset});
            }
          }

          if (!$stickElement.hasClass('sticky')) {
            // Header stickyness scroll effect.
            if (Drupal.ImportantInformation.is_scrolled_into_view(headerBlock) && !Drupal.ImportantInformation.is_scrolled_into_view(footerWrp)) {
              isiContBlock.css({height: windowHeight - headerHeight + windowTopPosition});
            }
          }
        }, 10));
      }
    }
  }

  // Throttle callbacks.
  Drupal.ImportantInformation.throttle = function(fn, wait){
    var time = Date.now();
    return function() {
      if ((time + wait - Date.now()) < 0) {
        fn();
        time = Date.now();
      }
    }
  }

  // Toggle the block display.
  Drupal.ImportantInformation.toggle_full_screen = function(){
    $('#block-important-information-important-information .action-links .full-screen').show().click(function(){
      if (!$('body').hasClass('important-information-full-screen')) {
        $('body').addClass('important-information-full-screen');
        var $buttonElement = $(this);
        // Replace the title of the block with a specified override.
        if (Drupal.settings.important_information.full_screen_title_override != "") {
          var $titleElement = $('.information-top-bar > h4 > span');
          var originalTitle = $titleElement.html();
          var newTitle = Drupal.settings.important_information.full_screen_title_override;
          if (Drupal.settings.important_information.full_screen_title_override == "(empty)") { 
            newTitle = "";
          }
          $titleElement.attr('data-original-title', originalTitle).html(newTitle);
        }
        // Replace the button contents.
        if ($buttonElement.attr('full-screen-open-text') != "") {
          $buttonElement
            .attr('full-screen-text', $buttonElement.html())
            .html($buttonElement.attr('full-screen-open-text'));
        }
        var oriFAClass = $buttonElement.attr('ori-fa-class');
        var backFAClass = $buttonElement.attr('back-fa-class');
        $buttonElement.removeClass(oriFAClass).addClass(backFAClass).addClass('clicked');
      } else {
        $('body').removeClass('important-information-full-screen');
        var $buttonElement = $(this);
        // Replace the title of the block with a specified override.
        if (Drupal.settings.important_information.full_screen_title_override != "") {
          var $titleElement = $('.information-top-bar > h4 > span');
          var originalTitle = $titleElement.attr('data-original-title');
          $titleElement.html(originalTitle);
        }
        // Replace the button contents.
        if ($buttonElement.attr('full-screen-open-text') != "") {
          $buttonElement
            .html($buttonElement.attr('full-screen-text'));
        }
        var oriFAClass = $(this).attr('ori-fa-class');
        var backFAClass = $(this).attr('back-fa-class');
        $buttonElement.removeClass(backFAClass).addClass(oriFAClass).removeClass('clicked');
      }
      $(window).resize();
    });
  }

  Drupal.ImportantInformation.is_scrolled_into_view = function (element) {
    var docViewTop = $(window).scrollTop();
    var window_height = $(window).height();
    var docViewBottom = (docViewTop + window_height);
    var elemTop = $(element).offset().top;
    var elemBottom = elemTop + $(element).outerHeight();
    var inView = false;
    if ((elemBottom >= docViewTop) &&
      (elemTop <= docViewBottom)) {
        inView = true;
    }
    return inView;
  }

  Drupal.ImportantInformation.toggle_relative_position = function (set_relative, set_auto_height = false) {
    var isiSelector = '#block-important-information-important-information';
    var $element = $(isiSelector);
    if (set_relative) {
      $element
        .css('width','100%')
        .css('position','relative')
        .css('bottom','0')
        .css('top','0')
        .css('left','0')
        .addClass('inline-element');
      if (Drupal.ImportantInformation.isiShowHeight == undefined || Drupal.ImportantInformation.isiShowHeight == "") {
        Drupal.ImportantInformation.isiShowHeight = $(window).scrollTop();
      }
      if (set_auto_height) {
        $element.css('height','auto');
      }
      $('body').removeClass('has-important-information');
    } else if ($element.attr('data-width') != "" 
    && Drupal.ImportantInformation.isiShowHeight > $(window).scrollTop()) {
      Drupal.ImportantInformation.isiShowHeight = "";
      $element
        .css('width','')
        .css('height','')
        .css('position','')
        .css('bottom','')
        .css('top','')
        .css('left','')
        .removeClass('inline-element');
      $('body').addClass('has-important-information');
    }
  }

  Drupal.behaviors.importantInformationModule = {
    attach: function(context, settings) {
      $('#block-important-information-important-information .action-links .dismiss', context).once('dismiss-important-information', function() {
        // Dismiss important information.
        if (settings.important_information.enable_dismiss == 1) {
          Drupal.ImportantInformation.toggle_display();
        }
      });
      $('#block-important-information-important-information .action-links .full-screen', context).once('full-screen-important-information', function() {
        // Full screen important information.
        if (settings.important_information.enable_full_screen == 1) {
          Drupal.ImportantInformation.toggle_full_screen();
        }
      });
      $('#block-important-information-important-information', context).once('important-information-block', function() {
        // Full screen important information.
        if (settings.important_information.enable_inline_bottom == 1) {
          var isiSelector = '#block-important-information-important-information';
          $(window).scroll(function() {
            var $element = $(isiSelector);
            if (Drupal.ImportantInformation.is_scrolled_into_view(settings.important_information.inline_bottom_selector)) {
              Drupal.ImportantInformation.toggle_relative_position(true, true);
            } else if ($element.attr('data-width') != "" 
            && Drupal.ImportantInformation.isiShowHeight > $(window).scrollTop()) {
              Drupal.ImportantInformation.toggle_relative_position(false);
            }
          });
        }
      });
    }
  };

  // Mobile breakpoints functionality to add classes to body.
  Drupal.ImportantInformationBreakpoints = {};

  // Add classes to body per browser width.
  Drupal.ImportantInformationBreakpoints.desktop_browser_width = function() {		
    set_width = function()	{
      winWidth=Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
      if(winWidth <= 767) {
        $('body').addClass('bw-mobile').removeClass('bw-tablet bw-desktop');
      }
      else if(winWidth > 767 && winWidth <= 999) {
        $('body').addClass('bw-tablet').removeClass('bw-mobile bw-desktop');
      }
      else if(winWidth >= 1000) {
        $('body').addClass('bw-desktop').removeClass('bw-tablet bw-mobile');
      }
    };
    $(window).resize(function () {
      set_width();
    });
    set_width();
  }

  Drupal.behaviors.importantInformationBlock = {
    attach: function (context, settings) {
      Drupal.ImportantInformationBreakpoints.desktop_browser_width();
      Drupal.ImportantInformation.stick_it();
    }
  };
  
})(jQuery);
